resource "aws_iam_instance_profile" "ec2_profile" {
  name = "${var.name}-ec2-profile"
  role = aws_iam_role.ec2_role.name
}

resource "aws_iam_role" "ec2_role" {
  name = "${var.name}-ec2-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}

variable "env_add_policies" {
  default = []
}

locals {
  policies = ["arn:aws:iam::aws:policy/AWSElasticBeanstalkWebTier", "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"]
  all_policies = setunion(local.policies, var.env_add_policies)
}

resource "aws_iam_role_policy_attachment" "test-attach" {
  count      = length(local.all_policies)
  role       = aws_iam_role.ec2_role.name
  policy_arn = tolist(local.all_policies)[count.index]
}