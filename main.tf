terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">=4.5.0"
    }
  }
}
variable "name" {}
variable "existing_app" {
  default = false
}

variable "app_desc" {
  default = ""
}

variable "env" {
  default = "dev"
}
variable "env_name_override" {
  default = ""
}
variable "environment_variables" {
  default = []
}

data "aws_elastic_beanstalk_solution_stack" "php" {
  most_recent = true

  name_regex = "^64bit Amazon Linux 2 (.*) running PHP 8.(.)$"
}

variable "env_solution_stack" {
  default = "64bit Amazon Linux 2 v3.3.11 running PHP 8.0"
}

resource "aws_key_pair" "main" {
  key_name   = "williamgrant-eb-access"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCQ0qpoTy0kGr4RFsOAia4DhLaJ91KWdS1NjE6uofdOv9/nb8F8kr7IMHQ5APuvk0Puw/OCA6+hlzBwifTsKrEHD/VPW5lCibX4C85SKVlF0uG3ackHNNOiD/jSsk47WIyTG5OigOqp2qUQhWaPWAD9GY7Dnk5ua3m5oa2qUU1M49cwv8TB6vW3dMYrrTwsfC0GDnZX0Ax15Q8uBCXshdbeBifzhMC0ojpaH7uvWWcG6Q3QGSv6pxu+UXz4uFLAgddT0msSls2lG/8KFHFUne2IWGEfYBlaxqm84oQ+Kd3hnBTTfeNIKJ3RpDDzv2YbW50NgknAFIM5Jm7JlzS7YE13 williamgrant"
}

resource "aws_elastic_beanstalk_application" "this" {
  count       = var.existing_app ? 0 : 1
  name        = var.name
  description = var.app_desc == "" ? "${var.name}-${var.env}" : var.app_desc

  tags = {
    app = var.name
    env = var.env
  }
}

resource "aws_elastic_beanstalk_environment" "this" {
  name                = var.env_name_override == "" ? "${var.name}-${var.env}" : var.env_name_override
  application         = var.existing_app ? var.name : aws_elastic_beanstalk_application.this[0].name
  solution_stack_name = var.env_solution_stack

  dynamic "setting" {
    for_each = local.settings
    content {
      name      = setting.value["name"]
      namespace = setting.value["namespace"]
      resource  = setting.value["resource"]
      value     = setting.value["value"]
    }
  }
}

output "url" {
  value = aws_elastic_beanstalk_environment.this.cname
}

resource "aws_elastic_beanstalk_configuration_template" "this" {
  name                = "${var.name}-${var.env}-config"
  application         = var.existing_app ? var.name : aws_elastic_beanstalk_application.this[0].name
  solution_stack_name = data.aws_elastic_beanstalk_solution_stack.php.name
}

module "canary" {
  source = "../terraform-aws-synth-canary"
  
  name           = "${var.name}-${var.env}-canary"
  monitoring_url = aws_elastic_beanstalk_environment.this.cname
}

locals {
  settings = setunion([
    {
      name      = "document_root"
      namespace = "aws:elasticbeanstalk:container:php:phpini"
      resource  = ""
      value     = "/public"
    },
    {
      name      = "memory_limit"
      namespace = "aws:elasticbeanstalk:container:php:phpini"
      resource  = ""
      value     = "1024M"
    },
    {
      name      = "ProxyServer"
      namespace = "aws:elasticbeanstalk:environment:proxy"
      resource  = ""
      value     = "nginx"
    },
    {
      name      = "InstanceTypes"
      namespace = "aws:ec2:instances"
      resource  = ""
      value     = "t3.medium"
    },
    {
      name      = "IamInstanceProfile"
      namespace = "aws:autoscaling:launchconfiguration"
      resource  = ""
      value     = aws_iam_instance_profile.ec2_profile.name
    },
    {
      name      = "EC2KeyName"
      namespace = "aws:autoscaling:launchconfiguration"
      resource  = ""
      value     = aws_key_pair.main.key_name
    },
    {
      name      = "MeasureName"
      namespace = "aws:autoscaling:trigger"
      resource  = ""
      value     = "CPUUtilization"
    },
    {
      name      = "Unit"
      namespace = "aws:autoscaling:trigger"
      resource  = ""
      value     = "Percent"
    },
    {
      name      = "UpperThreshold"
      namespace = "aws:autoscaling:trigger"
      resource  = ""
      value     = "80"
    },
    {
      name      = "LowerThreshold"
      namespace = "aws:autoscaling:trigger"
      resource  = ""
      value     = "40"
    },
    {
      name      = "DB_HOST"
      namespace = "aws:elasticbeanstalk:application:environment"
      resource  = ""
      value     = aws_db_instance.this.address
    },
    {
      name      = "DB_USERNAME"
      namespace = "aws:elasticbeanstalk:application:environment"
      resource  = ""
      value     = aws_db_instance.this.username
    },
    {
      name      = "DB_PASSWORD"
      namespace = "aws:elasticbeanstalk:application:environment"
      resource  = ""
      value     = random_string.db_password.result
    },
    {
      name      = "DB_PORT"
      namespace = "aws:elasticbeanstalk:application:environment"
      resource  = ""
      value     = "3306"
    },
    {
      name      = "DB_DATABASE"
      namespace = "aws:elasticbeanstalk:application:environment"
      resource  = ""
      value     = aws_db_instance.this.db_name
    }
  ], var.environment_variables)
}