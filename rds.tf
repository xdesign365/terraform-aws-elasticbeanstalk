variable "db_class" {
  default = "db.t3.micro"
}

variable "db_size" {
  default = 10
}

variable "db_autoscale" {
  default = true
}

resource "random_string" "db_password" {
  length           = 16
  special          = false
}

resource "aws_db_instance" "this" {
  identifier                = "${var.name}-${var.env}-db"
  allocated_storage         = var.db_size
  engine                    = "mysql"
  engine_version            = "5.7"
  instance_class            = var.db_class
  db_name                   = "presenter"
  username                  = "presenter"
  password                  = random_string.db_password.result
  parameter_group_name      = "default.mysql5.7"
  skip_final_snapshot       = false
  publicly_accessible       = true
  final_snapshot_identifier = "${var.name}-${var.env}-db-${formatdate("DDMMYY-hhmmss", timestamp())}"
}

output "db" {
  value = {
    host     = aws_db_instance.this.address
    username = aws_db_instance.this.username
    password = random_string.db_password.result
  }
}